<?php

define('medical_DEFAULT', '__default');
define('medical_EXPORT_VERSION', 8);

function medical_get_theme_dir() {
    return medical_normalize_path(__DIR__);
}

function medical_theme_suggestions_alter(array &$suggestions, array $variables, $hook) {
    if ($hook === 'html' || $hook === 'page') {
        $suggestions = medical_get_theme_suggestions($hook);
    }
}

function medical_preprocess(&$variables, $hook, $info) {
    /*$node  = medical_get_node(\Drupal::request()->attributes->get('node'));
    medical_var('theme_hook_suggestions', medical_get_template($variables, $node));*/
}

function medical_preprocess_html(&$variables) {
    $variables['theme_url']      = medical_get_theme_url();
    $variables['theme_css_url']  = medical_get_style_url();
    $variables['preview_js_url'] = medical_get_preview_js_url();
    $variables['version']        = medical_build_version_parameter();
    $variables['is_preview']     = medical_get_value('theme', false);
    
}

function medical_preprocess_page(&$variables) {
    $config     = \Drupal::config('system.site');
    $is_preview = medical_get_value('theme', false);
    $theme_dir  = medical_get_theme_dir();
    $theme_fso  = medical_generate_images_fso(medical_get_theme_url(), $theme_dir . '/images');
    
    if ($is_preview) {
        $base_theme = preg_replace('/(.*)(_preview$)/', '$1', 'medical');
        $base_theme_url = medical_get_base_url_with_slash() . drupal_get_path('theme', $base_theme);
        $base_theme_fso = medical_generate_images_fso($base_theme_url, $theme_dir . '/../' . $base_theme . '/images');
        $theme_fso = array_merge($theme_fso, $base_theme_fso);
    }

    $variables['logo']         = medical_get_logo_url();
    $variables['images']       = $theme_fso;
    $variables['site_name']    = $config->get('name');
    $variables['site_slogan']  = $config->get('slogan');
    $variables['theme_url']    = medical_get_theme_url();
    $variables['theme_dir']    = $theme_dir;
    $variables['includes_dir'] = $theme_dir . '/includes/';
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('search')){
        $variables['search_box']   = \Drupal::formBuilder()->getForm('Drupal\search\Form\SearchBlockForm');
    }
    $variables['is_preview']   = $is_preview;
    $variables['title']        = isset($variables['node'])? $variables['node']->title->value : $variables['page']['#title'];
    $variables['current_url']  = \Drupal::request()->getRequestUri();

    medical_restore_blog($variables);
}

function medical_preprocess_node(&$variables) {
    medical_restore_blog($variables);
    
    // https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Url.php/function/Url%3A%3AfromRoute/8.2.x
    // http://drupal.stackexchange.com/questions/144992/how-do-i-create-a-link
    // https://api.drupal.org/api/drupal/core!lib!Drupal.php/function/Drupal%3A%3Al/8.2.x
    $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $variables['node']->id()], ['attributes' => ['class' => ['art-comment']]]);
    $variables['add_comment'] = \Drupal::l(t('Add comment'), $url); // TODO: check permissions + comments count
}

function medical_preprocess_field(&$variables, $hook) {
    medical_restore_blog($variables);
}

function medical_preprocess_comment(&$variables) {
    medical_restore_blog($variables);
}

function medical_preprocess_toolbar(&$variables) {
    $variables['hide_toolbar'] = medical_get_value('theme', false);
}

function medical_preprocess_block(&$variables) {
    $theme_dir = medical_get_theme_dir();
    $block_id  = !empty($variables['elements']['#id']) ? $variables['elements']['#id'] : '';
    $block     = \Drupal\block\Entity\Block::load($block_id);

    if (!isset($block))
        return;

    $region    = $block->getRegion();

    $variables['shape_only'] = $block_id === 'medical_search' ? ' shape-only' : ''; // 6.x and 7.x
    $variables['region']     = !empty($region) && file_exists($theme_dir . '/templates/block/block--' . $region . '.html.twig') ? $region : '';

    // HMenu, VMenu, TabsMenu, TopMenu etc
    $process_regions = array('menu', 'breadcrumbs', 'language');
    foreach ($process_regions as $name) {
        if (strpos($region, $name . '_') === FALSE)
            continue;

        // just one VMenu in theme but several vmenu regions and HMenus
        $key = strpos($region, 'vmenu_') !== FALSE ? 'vmenu' : $region;

        require($theme_dir . '/templates/navigation/menu.php');
        if (isset($menus) && isset($menus[$key])) {
            $contentMarkupObj = drupal_render($variables['elements']['content']);
            $method = 'medical_process_' . $name;
            if (function_exists($method)) {
                $variables['content'] = $method($variables, $contentMarkupObj->__toString(), $menus[$key]);
            }
            // save template vars - for VMenu block etc.
            foreach ($menus[$key] as $name => $value) {
                $variables[$name] = $value;
            }
        }
    }
}

function medical_preprocess_pager(&$variables) {
    medical_restore_blog($variables);
}

function medical_preprocess_links(&$variables) {
    // http://drupal.stackexchange.com/questions/186175/edit-read-more-and-add-new-comment-links-in-teaser-in-drupal-8
    if ( isset($variables['links']['comment-add']['attributes']) && method_exists($variables['links']['comment-add']['attributes'], 'addClass') ) {
        // check tags field
    }
}

function medical_preprocess_breadcrumb(&$variables){
    if(($node = \Drupal::routeMatch()->getParameter('node')) && $variables['breadcrumb']){
        $variables['breadcrumb'][] = array(
            'text' => $node->getTitle() 
        );
    }
}

function medical_theme_suggestions_form_alter(array &$suggestions, array $variables) {
    if ($variables['element']['#form_id'] == 'search_block_form') {
        $suggestions[] = 'form__search_block_form';
    }
    if ($variables['element']['#id'] == 'search-block-form') {
        $suggestions[] = 'form__search_form';
    }
}

/**
 * Implements hook_form_alter() to add classes to the search form.
 */
function medical_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
    $id     = isset($form["#id"]) ? $form["#id"] : '';
    $config = \Drupal::configFactory()->getEditable('system.site');
    $config->set('form_id_attr', $id)->save();

    if (medical_get_value('theme', false)) {
        $action = $form["#action"];
        $flag = empty($action) || (strpos ($action, "theme=") !== FALSE) ||
            ((strpos($action, '#') !== FALSE) && (strpos($action, '/') === FALSE)); // Anchors with the id attribute
        if (!$flag) {
            $action = explode('?', $action);
            $action[0] =  $action[0] . '?theme=' . $_GET["theme"] . (sizeof($action) > 1 ? '&' : '');
            $form["#action"] = implode($action);
        }
    }

    if (in_array($form_id, ['search_block_form', 'search_form'])) {
        $key = ($form_id == 'search_block_form') ? 'actions' : 'basic';
        if (!isset($form[$key]['submit']['#attributes'])) {
            $form[$key]['submit']['#attributes'] = new Drupal\Core\Template\Attribute();
        }
        $theme_dir = medical_get_theme_dir();
        require($theme_dir . '/templates/search/search.php');

        $form_key = $id === 'search-block-form' && isset($search['search_form']) ? 'search_form' : 'search_block_form';
        $form['keys']['#attributes'] = array(
            'class' => array($search[$form_key]['input_class']),
            'placeholder' => array($search[$form_key]['input_placeholder'])
        );
        $form[$key]['submit']['#attributes']->addClass($search[$form_key]['button_class']);
        $config->set('search_button', drupal_render($form[$key]['submit']))->save();
    }
}

function medical_preprocess_form(&$variables) {
    $config = \Drupal::config('system.site');

    $variables['search_button'] = $config->get('search_button');
    $variables['form_id_attr']  = $config->get('form_id_attr');
}

function medical_preprocess_views_view_unformatted(&$variables) {
    medical_restore_blog($variables);
}