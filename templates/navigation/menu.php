<?php 
    if (!isset($menus)) $menus = array();
    $show_all = 'expand on click';
    $show_all_responsive = 'expand on click';
    $show_submenus = $show_all !== 'one level' || $show_all_responsive !== 'one level';
    $menus['vmenu'] = array(
        'effectsOpenTag'        => '',
        'effectsCloseTag'       => '',
        'classNames'            => ' bd-vmenu-3',
        'vmenuResponsive'       => "true",
        'vmenuResponsiveLevels' => $show_all_responsive,
        'vmenuBlockClassNames'  => ' bd-block',
        'vmenuBlockHeaderClassNames'   => ' bd-blockheader bd-tagstyles',
        'vmenuBlockContentClassNames'  => ' bd-blockcontent bd-tagstyles shape-only',
        'vmenuEffectsOpenTag'    => '',
        'vmenuEffectsCloseTag'   => '',
        'vmenuClassNames'        => ' bd-verticalmenu',
        
        'show_all'            => $show_all,
        'show_all_responsive' => $show_all_responsive,
        'show_submenus'       => $show_all !== 'one level' || $show_all_responsive !== 'one level',
        
        'menu_id'             => 'nav-pills-45',
        'menu_class'          => ' bd-menu-45 nav nav-pills',
        'menu_attr'           => '',
        'menu_head'           => '',
        'menu_tail'           => '',

        'menu_item_class'     => ' bd-menuitem-40',
        'menu_item_attr'      => '',
        'item_head'           => '',
        'item_tail'           => '',

        'submenu_popup'       => 'bd-menu-43-popup',
        'submenu_class'       => ' bd-menu-43',
        'submenu_attr'        => '',
        'submenu_head'        => '',
        'submenu_tail'        => '',

        'submenu_item_class'  => ' bd-menuitem-41',
        'submenu_item_attr'   => '',
        'submenu_item_head'   => '',
        'submenu_item_tail'   => '',

        'mega_category_item'    => '', // TODO
        'mega_subcategory_item' => '',
    );
?>
<?php 
    if (!isset($menus)) $menus = array();
    $show_all = 'expand on click';
    $show_all_responsive = 'expand on click';
    $show_submenus = $show_all !== 'one level' || $show_all_responsive !== 'one level';
    $menus['hmenu_3'] = array(
        'show_all'            => $show_all,
        'show_all_responsive' => $show_all_responsive,
        'show_submenus'       => $show_all !== 'one level' || $show_all_responsive !== 'one level',
        
        'menu_id'             => 'nav-pills-3',
        'menu_class'          => ' bd-menu-3 nav nav-pills navbar-left',
        'menu_attr'           => '',
        'menu_head'           => '',
        'menu_tail'           => '',

        'menu_item_class'     => ' bd-menuitem-4 bd-toplevel-item',
        'menu_item_attr'      => '',
        'item_head'           => '',
        'item_tail'           => '',

        'submenu_popup'       => 'bd-menu-4-popup',
        'submenu_class'       => ' bd-menu-4 ',
        'submenu_attr'        => '',
        'submenu_head'        => '',
        'submenu_tail'        => '',

        'submenu_item_class'  => ' bd-menuitem-5 bd-sub-item',
        'submenu_item_attr'   => '',
        'submenu_item_head'   => '',
        'submenu_item_tail'   => '',

        'mega_category_item'    => '', // TODO
        'mega_subcategory_item' => '',
    );
?>
<?php 
    if (!isset($menus)) $menus = array();
    $menus['breadcrumbs_1'] = array(
        'breadcrumb_classnames'      => ' bd-breadcrumbs-1',
        'breadcrumb_link_classnames' => ' bd-breadcrumbslink-1',
        'breadcrumb_text_classnames' => ' bd-breadcrumbstext-1 bd-no-margins',
    );
?>
<?php 
    if (!isset($menus)) $menus = array();
    $show_all = 'expand on click';
    $show_all_responsive = 'expand on click';
    $show_submenus = $show_all !== 'one level' || $show_all_responsive !== 'one level';
    $menus['tabsmenu_1'] = array(
        'show_all'            => $show_all,
        'show_all_responsive' => $show_all_responsive,
        'show_submenus'       => $show_all !== 'one level' || $show_all_responsive !== 'one level',
        
        'menu_id'             => 'nav-pills-75',
        'menu_class'          => ' bd-menu-75 nav nav-pills navbar-left',
        'menu_attr'           => '',
        'menu_head'           => '',
        'menu_tail'           => '',

        'menu_item_class'     => ' bd-menuitem-55',
        'menu_item_attr'      => '',
        'item_head'           => '',
        'item_tail'           => '',

        'submenu_popup'       => 'bd-menu-59-popup',
        'submenu_class'       => ' bd-menu-59',
        'submenu_attr'        => '',
        'submenu_head'        => '',
        'submenu_tail'        => '',

        'submenu_item_class'  => ' bd-menuitem-57',
        'submenu_item_attr'   => '',
        'submenu_item_head'   => '',
        'submenu_item_tail'   => '',

        'mega_category_item'    => '', // TODO
        'mega_subcategory_item' => '',
    );
?>