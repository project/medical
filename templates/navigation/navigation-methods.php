<?php

function medical_process_language(&$variables, $content, $menu) {
    $title  = $menu['show_label'] && !empty($variables['configuration']['label_display']) ? $variables['configuration']['label'] . ':&nbsp;' : '';
    $result = $menu['head'] .
              $menu['language_head'] .
              $title .
              medical_get_language($menu['text_type']) .
              $menu['language_content'] .
              $content .
              $menu['language_tail'] .
              $menu['tail'];

    return medical_process_menu($variables, $result, $menu);
}

function medical_process_breadcrumbs(&$variables, $content, $menu) {
    if (!empty($content)) {
        foreach ($menu as $name => $value) {
            $content = str_replace($name, $value, $content);
        }
    }
    return $content;
}

function medical_process_menu(&$variables, $content, $menu) {
    if (empty($content) || !class_exists('DOMDocument') ) {
        return;
    }

    //preg_match_all("#([^\s]+)=([^\s]+)#s", $menu_item_attr, $out); 
    $class_ptrn    = '/(.*?<ul.*?class=[\'"])(.*?)([\'"])/i';
    $no_class_ptrn = '/(.*?<ul)(.*?)>/i';
    if (preg_match($class_ptrn, $content, $matches)) {
        $output = preg_replace($class_ptrn, '$1'.$menu['menu_class'].' $2$3', $content);
    } elseif (preg_match($no_class_ptrn, $content)) {
        $output = preg_replace($no_class_ptrn, '$1 class="'.$menu['menu_class'].'" $2>', $content);
    }
    $output = medical_menu_xml_parcer($output, $menu);
    /* Support Block Edit Link module */
    $result = str_replace('<!DOCTYPE root>', '', $output);
    return $result;
}

function medical_menu_xml_parcer($content, $menu) {
    $parent_id = $menu['menu_id'] . '-id';

    $doc = medical_xml_document_creator($content, $parent_id);
    if ($doc === FALSE) {
        return $content; // An error occurred while reading XML content
    }

    $parent = $doc->documentElement;
    $elements = $parent->childNodes;
    $ul_elements = $doc->getElementsByTagName("ul");
    if ($ul_elements == NULL || !$ul_elements->length)
        return $content;

    $ul = NULL;
    foreach($ul_elements as $ul) {
        if (strpos($ul->getAttribute('class'), $menu['menu_class']) !== FALSE)
            break;
        continue;
    }

    if (!isset($ul))
        $ul = $ul_elements->item(0);

    if (!empty($menu['menu_item_class'])) { // Don't use active class for first level <li> in Language menu etc.
        medical_menu_set_active($doc, $menu['menu_class']);
    }
    medical_menu_style_parcer($doc, $ul->childNodes, $menu);

    $parent->appendChild($ul);
    while ($ul->previousSibling)
        $parent->removeChild($ul->previousSibling);

    $children = $parent->childNodes;
    $inner_HTML = '';
    foreach ($children as $child) {
        $tmp_doc = new DOMDocument();
        $tmp_doc->appendChild($tmp_doc->importNode($child,true));       
        $inner_HTML .= $tmp_doc->saveHTML();
    }

    return html_entity_decode($inner_HTML, ENT_NOQUOTES, "UTF-8");
}

function medical_xml_document_creator($content, $parent_id) {
    $cleared_content = preg_replace('/(?=<!--)([\s\S]*?)-->/', '', $content); // http://stackoverflow.com/questions/1084741/regexp-to-strip-html-comments
    $old_error_handler = set_error_handler('medical_handle_xml_error');
    $dom = new DOMDocument();
    /* Support Block Edit Link module */
    $doc_content = <<< XML
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE root [
<!ENTITY nbsp "&#160;">
]>
<div class="$parent_id">$cleared_content</div>
XML;
 
    $dom->loadXml($doc_content);   
    restore_error_handler();
    return $dom;
}

function medical_handle_xml_error($errno, $errstr, $errfile, $errline) {
    if ($errno==E_WARNING && (substr_count($errstr,"DOMDocument::loadXML()")>0))
        return false; // An error occurred while reading XML content
    else 
        return true; // Successful
}

function medical_menu_style_parcer($doc, $elements, $menu, $level = 0) {
    $to_delete = array();

    foreach ($elements as $element) {
        if (is_a($element, "DOMElement") && ($element->tagName == "li")) {
            $liClass  = ($level > 0  ? $menu['submenu_item_class'] : $menu['menu_item_class']) . ' ' . $element->getAttribute('class');
            $liClass .= $level === 1 ? $menu['mega_category_item'] : ($level === 2 ? $menu['mega_subcategory_item'] : '');
            $element->setAttribute('class', $liClass);
            $children = $element->childNodes;

            foreach ($children as $child) {
                if (is_a($child, "DOMElement") && ($child->tagName == "a")) {
                    $caption = $child->nodeValue;
                    if (empty($caption)) {
                        $to_delete[] = $child;
                        $to_delete[] = $element;
                        break;
                    }
                }
                elseif ($menu['show_submenus'] && is_a($child, "DOMElement") && ($child->tagName == "ul")) {
                    //submenus
                    $child->setAttribute('class', $menu['submenu_class']);
                    medical_menu_style_parcer($doc, $child->childNodes, $menu, (int)$level + 1);
                    $element->setAttribute('class', $liClass . ' bd-submenu-icon-only');
                    $div_popup = $doc->createElement('div');
                    $div_popup->setAttribute('class', $menu['submenu_popup']);
                    $div_popup->appendChild($child);
                    $element->appendChild($div_popup);
                    $to_delete = $child;
                }
                elseif (!$menu['show_submenus']) {
                    $to_delete[] = $child;
                }
            }
        }
    }

    medical_remove_elements($to_delete);
    return $elements;
}

function medical_menu_set_active($doc, $menu_class) {
    $xpath = new DOMXPath($doc);
    $query = "//ul[contains(concat(' ',@class,' '), ' $menu_class ')]//a[contains(concat(' ',@class,' '), ' active ')]";
    $items = $xpath->query($query, $doc);
    if ($items->length == 0) return;

    $active_a = $items->item(0);
    //$query = "ancestor::li[contains(concat(' ',@class,' '), ' active-trail ')]";
    $query = "ancestor::li";
    $items = $xpath->query($query, $active_a);

    foreach ($items as $item) {
        $class_attr = $item->getAttribute("class");
        /*if (strpos(' '.$class_attr.' ', ' active ') === FALSE) { // DR-12140
            $item->setAttribute("class", $class_attr.' active');
        }*/

        $children = $item->childNodes;
        foreach($children as $child) {
            if (is_a($child, "DOMElement")) {
                $class_attr = $child->getAttribute("class");
                if (strpos(' '.$class_attr.' ', ' active ') === FALSE) {
                    $child->setAttribute("class", $class_attr.' active');
                }
            }
        }
    }
}

function medical_remove_elements($elements_to_delete) {
    if (!isset($elements_to_delete))
        return;
    foreach($elements_to_delete as $element) {
        if ($element != null) {
            $element->parentNode->removeChild($element);
        }
    }
}