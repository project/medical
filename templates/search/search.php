<?php 
    if (!isset($search)) $search = array();
    $search['search_block_form'] = array(
        'button_class'      => ' bd-button-7',
        'button_attr'       => '',
        'input_class'       => ' bd-bootstrapinput form-control',
        'input_attr'        => '',
        'input_placeholder' => 'Search',
    );
?>