<?php 
    if (!isset($templates)) $templates = array();
    $templates['blog'][] = array('id' => '5', 'ui_title'=> 'Blog Template', 'd8' => 'blog__5', 'filename' => 'blog--5', 'name' => 'blogTemplate', 'caption' => "Blog");
?>
<?php 
    if (!isset($templates)) $templates = array();
    $templates['default'][] = array('id' => '2', 'ui_title'=> 'Default Template', 'd8' => 'default__2', 'filename' => 'default--2', 'name' => 'default', 'caption' => "Default");
?>
<?php 
    if (!isset($templates)) $templates = array();
    $templates['home'][] = array('id' => '1', 'ui_title'=> 'Home Template', 'd8' => 'front__1', 'filename' => 'front--1', 'name' => 'home', 'caption' => "Home");
?>
<?php 
    if (!isset($templates)) $templates = array();
    $templates['pageTemplate'][] = array('id' => '11', 'ui_title'=> 'Page Template', 'nodeType' => 'page',  'd8' => 'node__11', 'filename' => 'node--11', 'name' => 'pageTemplate', 'caption' => "Page");
?>
<?php 
    if (!isset($templates)) $templates = array();
    $templates['singlePostTemplate'][] = array('id' => '6', 'ui_title'=> 'Post Template', 'nodeType' => 'article', 'd8' => 'node__article__6', 'filename' => 'node--article--6', 'name' => 'singlePostTemplate', 'caption' => "Post");
?>
<?php 
    if (!isset($templates)) $templates = array();
    $templates['template404'][] = array('id' => '9', 'ui_title'=> '404 Template', 'd8' => 'template404__9', 'filename' => 'template404--9', 'name' => 'template404', 'caption' => "404");
?>