<?php

function medical_restore_blog(&$variables) {
    require(medical_get_theme_dir() . '/templates/node/blog.php');
    $suggestions = medical_get_theme_suggestions();
    $blog_id     = medical_get_blog_id($suggestions);

    $variables['blog_id']    = $blog_id;
    $variables['blog_items'] = $blogs[$blog_id];
}

function medical_get_blog_id($suggestions) {
    $theme_dir = medical_get_theme_dir();
    $blog_id   = NULL;
    foreach ($suggestions as $name) {
        $path = $theme_dir . '/templates/page/' . preg_replace('/__+/', '--', $name) . '.html.twig';
        if (file_exists($path)) {
            $content = file_get_contents($path);
            $blog_id = preg_match('#(blogId)=([^\s]+)#s', $content, $matches) && isset($matches[2]) ? $matches[2] : $blog_id;
        }
    }
    // no blog id in suggested template
    if (!isset($blog_id)) {
        require($theme_dir . '/templates/node/blog.php');
        if (!empty($default_blog_id) && file_exists($theme_dir.'/includes/blog_' . $default_blog_id . '.html.twig')) {
            $blog_id = $default_blog_id;
        }
    }
    return $blog_id;
}

function medical_get_theme_suggestions($base_name = 'page') {
    $suggestions = array();

    $backwardDefaultTemplate = medical_get_backward_template();
    $default = $base_name . ($backwardDefaultTemplate ?  '__'. $backwardDefaultTemplate : medical_DEFAULT);
    $suggestions[] = $default;

    $request = \Drupal::request();
    $path    = \Drupal::service('path.current')->getPath();
    $alias   = \Drupal::service('path.alias_manager')->getAliasByPath($path);
    $args    = explode('/', $path);

    $node = medical_get_node($request->attributes->get('node'));
    if (isset($node) && is_object($node) && $node->id() && (!isset($args[3]) || ($args[3] !== 'edit'))) {
        // if there is no page template file for $node->type, page--node template used
        $suggestions[] =  $base_name . '__node';
        $node_type = $node->getType();
        if (!empty($node_type)) {
            $suggestions[] =  $base_name . '__node__' . $node_type;
            $type = medical_get_predefined_template($node_type);
        }
    }

    if (count($args) == 2 && $args[1] === 'node') {
        $type = 'blog';
        $suggestions[] =  $base_name .'__'.$type;
    }
    if (\Drupal::service('path.matcher')->isFrontPage()) {
        $type = 'home';
        $suggestions[] =  $base_name .'__front';
    }
    
    if ($exception = $request->attributes->get('exception')) {
        $code = $exception->getStatusCode();
        if (in_array($code, array(403, 404))) {
            $type = 'template404';
            $suggestions[] = $base_name . '__' .$type;
        }
    }

    if (!empty($type)) { // templates from theme settings
        $backwardTemplate = medical_get_backward_template($type);
        if ($backwardTemplate) {
            $suggestions[] =  $base_name .'__'. $backwardTemplate;
        }
        $var = $data = \Drupal::state()->get(medical_get_template_key($type)) ?: false;
        if ($var) {
            $suggestions[] =  $base_name .'__'. $var;
        }
    }

    $is_preview = medical_get_value('theme', false); // to support custom templates in preview
    $customTemplateVarName = medical_get_custom_template_variable_name($alias, $is_preview);

    $var = \Drupal::state()->get($customTemplateVarName, false); // page with the URL, assigned to custom template
    medical_set_custom_template($suggestions, $var, $base_name);

    $template = medical_get_value('template'); // preview URL with template parameter
    medical_set_custom_template($suggestions, $template, $base_name);

    if (($args[0] === 'notemplate') && isset($args[1])) {
        $suggestions['notemplate'] = $args[1];
        $suggestions[] = $base_name . '__notemplate';
    }

    return $suggestions;
}

function medical_get_backward_template($type = 'default') {
    $templates = medical_get_templates();
    if (isset($templates) && array_key_exists($type, $templates)) {
        foreach ($templates[$type] as $template) {
            if (isset($template['name']) && (($template['name'] == $type) ||
                (($type == 'blog') && ($template['name'] == 'blogTemplate')))) { // blogTemplate is default name for blog type template
                return $template['d8'];
            }
        }
    }
    return false;
}

function medical_get_templates() {
    $path = medical_get_theme_dir().'/templates/templates.php';;
    if (file_exists($path)) {
        require($path);
        return $templates;
    }
    return array();
}

function medical_get_custom_template_urls_list_key() {
    return 'medical_'.medical_get_custom_template_url_key().'_list';
}

function medical_get_custom_template_url_key() {
    return 'customtemplateUrl';
}

function medical_get_custom_template_variable_name($url, $base_theme_name = false) {
    $name = $base_theme_name ? $base_theme_name : 'medical'; // to support custom templates in preview
    return $name .'_'. medical_get_custom_template_url_key() . '_' . preg_replace('/[^A-Za-z0-9]/', '', $url);
}

function medical_set_custom_template(&$suggestions, $template, $base_name) {
    if (empty($template)) {
        return;
    }

    $suggestions[] =  $base_name . '__'.$template;
    $custom_template = medical_get_custom_template_filename($template);
    if (!empty($custom_template)) {
        $suggestions[] =  $base_name .'__'. $custom_template;
    }
}

function medical_get_custom_template_filename($value) {
    $version = medical_get_major_version();
    $templates = medical_get_templates();
    if (!empty($templates) && isset($templates['customTemplate'])) {
        foreach ($templates['customTemplate'] as $customTemplate) {
            if (isset($customTemplate['custom_name_d'.$version]) && isset($customTemplate['d'.$version]) && ($value == $customTemplate['d'.$version])) {
                return $customTemplate['custom_name_d'.$version];
            }
        }
    }
    return false;
}

function medical_get_template_key($type) {
    return 'medical_'.$type;
}

function medical_var($var_name, $new_val = NULL) {
    $vars = &drupal_static(__FUNCTION__, array());
    if ($new_val) {
        $vars[$var_name] = $new_val;
    }
    return isset($vars[$var_name]) ? $vars[$var_name] : NULL;
}

function medical_get_node($node) {
    return isset($node) && is_object($node) ? $node : NULL;
}

function medical_get_logo_url() {
    $logo = theme_get_setting('logo');
    if ($logo['use_default']) {
        $logo_path = medical_get_theme_dir() . '/logo.svg';
        $logo_url =  file_exists($logo_path) ? $logo['url'] : '';
    } else {
        $logo_path = drupal_realpath($logo['path']);
        $logo_url  = file_exists($logo_path) ? file_create_url($logo['path']) : '';
    }
    return $logo_url;
}

function medical_get_language($text_type = 'short') {
    $language = \Drupal::languageManager()->getCurrentLanguage();
    switch ($text_type) {
        case 'full':
            return $language->getName();
        case 'noText':
            return '';
        case 'short':
        default:
            return $language->getId();
    }
}

function medical_get_predefined_template($node_type) {
    switch ($node_type) {
        case 'article':
            $type = 'singlePostTemplate';
            break;
        case 'page':
            $type = 'pageTemplate';
            break;
        case 'forum':
            $type = 'forumTemplate';
            break;
        default:
            $type = false;
            break;
    }
    return $type;
}