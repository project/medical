<?php

use \Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Url;

function medical_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
    if (preg_match('#_preview$#', 'medical', $matches))
        return; // base theme only, can be downloaded without project file

    $templates = medical_get_templates();
    $version   = medical_get_major_version();

    $form['templates']  = array(
        '#type' => 'fieldset',
        '#weight' => '-3',
        '#title' => t('Template settings')
    );
    $form['templates']['settings'] = medical_render_templates_options($templates, $version);
    $form['templates']['save_templates'] = array(
        '#type' => 'submit',
        '#value' => t('Save')
    );
    $form['templates']['save_templates']['#submit'][] = 'medical_save_templates_submit';

    $custom_templates = medical_render_custom_templates_form($templates, $version);
    if (!empty($custom_templates)) {
        $form['customtemplates'] =  array(
            '#type' => 'fieldset',
            '#weight' => '-2',
            '#title' => t('Custom page template URLs')
        );
        $form['customtemplates']['settings'] = $custom_templates;
        $form['customtemplates']['clear_templates'] = array(
            '#type' => 'submit',
            '#value' => t('Clear')
        );
        $form['customtemplates']['clear_templates']['#submit'][] = 'medical_clear_templates_submit';
    }

    if (function_exists('themler_show')) { // Installed Themler only
        $form['themler']  = array(
            '#type' => 'fieldset',
            '#weight' => '-1',
            '#title' => t('Themler')
        );
          
        $form['themler']['edit_theme'] = array(
            '#type' => 'submit',
            '#value' => t('Edit theme')
        );
        $form['themler']['edit_theme']['#submit'][] = 'medical_edit_theme_submit';
    }
}

function medical_get_state() {
    return \Drupal::state();
}

function medical_edit_theme_submit(array &$form, FormStateInterface $form_state) {
    $theme_name = 'medical';
    $version = theme_get_manifest_version($theme_name);
    $form_state->setRedirect('themler.show', ['theme' => $theme_name, 'ver' => $version]);
}

function medical_render_templates_options($templates, $version) {
    $result = array();
    $state  = medical_get_state();
    foreach ($templates as $type => $template) {
        $options = array();
        if (isset($templates[$type])) {
            $title = '';
            foreach ($template as $t) {
                $options[$t['d'.$version]] = $t['caption'];
                $title = $t['ui_title'];
            }

            $defaultValue = $state->get(medical_get_template_key($type), false);
            if (!$defaultValue) { // backward: template type might not be set yet
                $defaultValue = medical_get_backward_template($type);
            }

            $result[$type] = array(
                '#type' => 'select',
                '#options' => $options,
                '#title' => $title,
                //'#description' => t('Choose "'.$type.'" to always use on the '.$type.' page.'),
                '#default_value' => $defaultValue,
            );
            if ($type === 'customTemplate') {
                $result[medical_get_custom_template_url_key()] = array(
                    '#type' => 'textfield',
                    '#title' => t('Custom page template URL'),
                    '#description' => t('Input page URL you wish to use custom template.'),
                    '#default_value' => '',
                    '#field_prefix' => Url::fromRoute('<front>'), // TODO: 8.x
                ); 
            }
        }
    }

    return $result;
}

function medical_render_custom_templates_form($templates, $version) {
    if (!isset($templates['customTemplate']))
        return;

    $result = array();
    $state  = medical_get_state();
    $name   = medical_get_custom_template_urls_list_key();

    $allCustomUrls = $state->get($name, false);
    if ($allCustomUrls) { // build list of all custom urls
        $explode = explode(',', $allCustomUrls);
        $unique  = array_unique($explode);
        $options = array();
        foreach ($unique as $u) {
            $url = $state->get($u.'_url', false);
            $t   = $state->get($u, false);
            
            $customTemplateVarName = medical_get_custom_template_variable_name($url);
            $customTemplateName    = $state->get($customTemplateVarName, false);

            $caption = '';
            foreach ($templates['customTemplate'] as $customTemplate) {
                if (isset($customTemplate['d'.$version]) && ($customTemplateName == $customTemplate['d'.$version])) {
                    $caption = $customTemplate['caption'];
                }
            }

            $options[$caption][$u] = $url;
        }
    
        if (!empty($options)) {
            $result[$name] = array(
                '#type' => 'select',
                '#options' => $options,
                '#description' => t('List of page URLs using custom templates.'),
                '#default_value' => '',
            );
        }
    }
    
    return $result;
}

function medical_save_templates_submit(array &$form, FormStateInterface $form_state) {
    $state  = medical_get_state();

    $customTemplateUrlKey = medical_get_custom_template_url_key();
    $templates = medical_get_templates();
    foreach ($templates as $type => $template) {
        if ($value = $form_state->getValue($type)) {
            $state->set(medical_get_template_key($type), $value);
        }
    }
    if ($value = $form_state->getValue($customTemplateUrlKey)) {
        $path     = \Drupal::service('path.alias_manager')->getPathByAlias($value);
        $is_valid = \Drupal::service('path.validator')->isValid($path);
        if (!$is_valid) {
            drupal_set_message(t("The path '$path' is either invalid or you do not have access to it."), 'error');
        } else {
            $key = medical_get_custom_template_variable_name($value);
            $state->set($key, $form_state->getValue('customTemplate'));
            $state->set($key.'_url', $value);

            medical_save_custom_template_urls_list($key);
            drupal_set_message('Templates configuration has been saved');
        }
    } else {
        drupal_set_message(t("Incorrect template values."), 'error');
    }
    //drupal_flush_all_caches();
}

function medical_clear_templates_submit(array &$form, FormStateInterface $form_state) {
    $state  = medical_get_state();
    $name   = medical_get_custom_template_urls_list_key();

    if ($value = $form_state->getValue($name)) {
        $state->delete($value); 
        $state->delete($value.'_url');
        
        $allCustomUrls = $state->get($name, '');
        $explode = explode(',', $allCustomUrls);
        $unique  = array_unique($explode);
        if (($key = array_search($value, $unique)) !== false) {
            unset($unique[$key]);
        }
        $implode = implode(',', $unique);
        $state->set($name, $implode);
    }
}

function medical_save_custom_template_urls_list($urlKey) {
    // save all custom template urls
    $state = medical_get_state();
    $name  = medical_get_custom_template_urls_list_key();
    $allCustomUrls = $state->get($name, '');
    $allCustomUrls .= ($allCustomUrls ? ',' : '') . $urlKey;
    
    $explode = explode(',', $allCustomUrls);
    $unique  = array_unique($explode);
    $implode = implode(',', $unique);

    $state->set($name, $implode);
}