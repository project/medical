===========
Contents
===========

* Installing Drupal Themes

*** Installing Drupal Themes 
---------------------------------------
1. Access your Web server using an FTP client or Web server administration tools.
2. Create a folder for your specific theme under "<YourSiteFolder>/themes/" folder within Drupal installation.
   For example: <YourSiteFolder>/themes/<MyNewTheme>
3. Copy or upload theme files exported from Themler into the newly created <MyNewTheme> folder.
4. Login to your Drupal Administer.
5. Go to Drupal Administer -> Site Building -> Themes (www.YourSite.com/?q=admin/build/themes)
6. Select your newly uploaded theme from the list of available themes for your site.
7. Click the "Save configuration" button to save your changes.
For more information please visit: http://drupal.org/node/456